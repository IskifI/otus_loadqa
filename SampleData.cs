using System;
using System.Linq;
using otus_loadqa.Models;

namespace otus_loadqa
{
    public class SampleData
    {
        public static void Initialize(MobileContext context)
        {
            if (!context.Phones.Any())
            {
                context.Phones.AddRange(
                    new Phone
                    {
                        Name = "iPhone X",
                        Company = "Apple",
                        Price = 600
                    },
                    new Phone
                    {
                        Name = "Samsung Galaxy Edge",
                        Company = "Samsung",
                        Price = 550
                    },
                    new Phone
                    {
                        Name = "Pixel 3",
                        Company = "Google",
                        Price = 500
                    }
                );
                context.SaveChanges();
            }

            if (!context.Clients.Any())
            {
                context.Clients.AddRange(
                    new Client
                    {
                        Email = "User_1@email.com",
                        Password = "Q1w2e3"
                    },
                    new Client
                    {
                        Email = "User_2@email.com",
                        Password = "Q1w2e3"
                    },
                    new Client
                    {
                        Email = "User_3@email.com",
                        Password = "Q1w2e3"
                    }
                );
                context.SaveChanges();
            }
        }
    }
}