using Microsoft.EntityFrameworkCore;

namespace otus_loadqa.Models
{
    public class MobileContext : DbContext
    {
        public DbSet<Client> Clients { get; set; }
        public DbSet<Phone> Phones { get; set; }
        public DbSet<Order> Orders { get; set; }
        
        public MobileContext(DbContextOptions<MobileContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
    }
}