﻿using System.Diagnostics;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using otus_loadqa.Models;

namespace otus_loadqa.Controllers
{
    public class HomeController : Controller
    {
        MobileContext db;
        public HomeController(MobileContext context)
        {
            db = context;
        }

        [Authorize]
        public IActionResult Index()
        {
            return View(db.Phones.ToList());
        }

        [HttpGet]
        public IActionResult Buy(int? id)
        {
            if (id == null) return RedirectToAction("Index");
            ViewBag.PhoneId = id;
            return View();
        }

        [HttpPost]
        public string Buy(Order order)
        {
            db.Orders.Add(order);
            // сохраняем в бд все изменения
            db.SaveChanges();
            return "Спасибо за покупку, " + order.User + "!";
        }

        /* string guid = Guid.NewGuid().ToString();
        public IActionResult Index(string id, string param)
        {
            if (id == string.Empty)
            {
                ViewBag[0] = guid;
                return View();
            }
            else if (id != string.Empty && id == guid)
            {
                if (param != string.Empty)
                {
                    ViewBag[0] = "Вы ничего не искали";
                    return View();
                }
                else
                {
                    ViewBag[0] = param;
                    return View();
                }
                
            }
            else
            {
                ViewBag[0] = "GUID сессии не совпал!";
                return View();
            }
        }*/

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
